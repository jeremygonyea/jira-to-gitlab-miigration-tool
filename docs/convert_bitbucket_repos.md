# Bitbucket Namespace Conversion

Bitbucket stores its code repositories in numerically assigned bare repos on disk.  The folder numbers are referenced within its database, reachable externally via Bitbucket's API.

## Setup - Installation
Install the python requirements:

`pip install -r requirements.txt`

Alternatively, a VSCode docker development environment is included in this repo, and can be built automatically to run this project.

## Setup - Configuration

Copy example configuration files and replace the K-V pairs with your own settings.  See below for descriptions on what is stored in each file.
* PROJECT_ROOT/example/.env => PROJECT_ROOT/.env

### .env:
* username/password: The default username/ password that will connect to the Bitbucket instance.  This user should have read capabilty to all of the Bitbucket projects/repos you intend to access.
* bitbucketBaseURL: URL of the source Bitbucket instance.
  
## Migration Steps
* Configure a new git server following the instructions from their [online instructions](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server).
* Rsync/ copy to a new server the repos data, located in `BITBUCKET-HOME/shared/data/repositories`.  Don't forget to change permissions/ ownership to the `git` user as needed.
* Run the python script `convert_bitbucket_repos.py` from this project's root.  A bash script will be generated in the `data` folder.
* Copy this script to the new rsync'd directory and run it.  Warnings of folders with names `.` and `..` not being able to be moved are expected.
* All folders should now be moved to their appropriate namespaced locations.  

## Notes
* Any remaining numbered folders after running the generated script should be investigated to ensure no data loss.
* This script assumes that there are no repos that are purely numbers.  If there are, data loss will most likely occur on the rsync'd folder by running this script.