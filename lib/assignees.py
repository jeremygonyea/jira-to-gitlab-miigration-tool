""" Helps convert jira usernames to gitlab user id's """
import json
import gitlab
from dotenv import dotenv_values

with open('data/users.json', 'r', encoding="UTF-8") as users:
    gl_users = json.load(users)

config = dotenv_values(".env")

def load_gitlab_ids():
    """ Preloads gitlab id's via Gitlab API for future lookups """
    gl_api = gitlab.Gitlab(url=config["gitlabURL"], private_token=config["gitlab"])

    for gl_user in gl_users['assignees']:
        if "gitlabID" not in gl_users['assignees'][gl_user]:
            user = gl_api.users.list(username=gl_users['assignees'][gl_user]['gitlabUser'])[0]
            gl_users['assignees'][gl_user]['gitlabID'] = user.id

    with open('data/users.json', 'w', encoding="UTF-8") as output_file:
        json.dump(gl_users, output_file, indent=4)

def get_gl_id_by_name(assignee) -> int:
    """ Performs gitlab user id lookup """
    if assignee is None:
        jira_name = config['username']
    else:
        jira_name = assignee.name.lower()

    # Default to unassigned gitlab ID.
    result = 0

    if jira_name in gl_users["assignees"]:
        result = gl_users["assignees"][jira_name]['gitlabID']
    else:
        result = get_gl_id_by_name(None)

    return result

def get_gl_pat_by_name(assignee: str) -> str:
    """ Returns user's gitlab pat """

    # Default to .env configuration.
    result = config['gitlab']

    if assignee is not None:
        jira_name = assignee.name.lower()
        if jira_name in gl_users["assignees"]:
            result = gl_users["assignees"][jira_name]['gitlabPAT']

    return result

def get_all_users():
    """ Returns list of JIRA users """
    return gl_users['assignees']

load_gitlab_ids()
